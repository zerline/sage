# Sage version information for shell scripts
# This file is auto-generated by the sage-update-version script, do not edit!
SAGE_VERSION='8.5'
SAGE_RELEASE_DATE='2018-12-22'
SAGE_VERSION_BANNER='SageMath version 8.5, Release Date: 2018-12-22'
